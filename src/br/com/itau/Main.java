package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Simulacao simulacao = new Simulacao();
        Cliente cliente = new Cliente();

        Escaner.obterDadosSimulacao(simulacao, cliente);
        Impressora.exibirDadosSimulacao(simulacao);

    }
}
