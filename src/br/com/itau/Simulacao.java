package br.com.itau;

public class Simulacao {

    private Cliente cliente;

    public Investimento getInvestimento() {
        return investimento;
    }

    private Investimento investimento;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    private double valorCapitalAInvestir;
    private int quantidadeMeses;


    public double getValorCapitalAInvestir() {
        return valorCapitalAInvestir;
    }

    public void setValorCapitalAInvestir(double valorCapitalAInvestir) {
        this.valorCapitalAInvestir = valorCapitalAInvestir;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public double calcularSimulacao(){
        return this.getInvestimento().calcularRetornoInvestimento(this.valorCapitalAInvestir, this.quantidadeMeses);

    }

    public Simulacao() {
        this.investimento = new Investimento();
    }
}
