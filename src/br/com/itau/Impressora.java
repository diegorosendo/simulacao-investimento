package br.com.itau;

import java.util.Scanner;

public class Impressora {

    public static void exibirDadosSimulacao(Simulacao simulacao){
        System.out.println("--------------------------------");
        System.out.println("..:: Resultado da Simulação ::..");
        System.out.println("--------------------------------");
        System.out.println("Nome Cliente: "+ simulacao.getCliente().getNome());
        System.out.println("Valor a ser investido: "+ simulacao.getValorCapitalAInvestir());
        System.out.println("Quantidade de meses: "+ simulacao.getQuantidadeMeses());
        System.out.println("Taxa mensal retorno investimento: "+ simulacao.getInvestimento().getTaxa());
        System.out.println("O montante acumulado será:" + simulacao.calcularSimulacao());
    }

}
