package br.com.itau;

public class Investimento {

    private double taxa;

    public double getTaxa() {
        return taxa;
    }


    public Investimento() {
        this.taxa = 0.007;
    }

    public double calcularRetornoInvestimento(double valorCapital, int quantidadeMeses) {
        double rendimento = 0;
        double acumulado = valorCapital;
        for (int i = 0; i < quantidadeMeses; i++) {
            rendimento = acumulado * this.taxa;
            acumulado += rendimento;
        }
        return acumulado;
    }
}