package br.com.itau;

import java.util.Scanner;

public class Escaner {
    public static void obterDadosSimulacao(Simulacao simulacao, Cliente cliente){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o nome do Cliente: ");
        cliente.setNome(scanner.next());
        simulacao.setCliente(cliente);

        System.out.println("Informe o valor do capital a ser investido: ");
        simulacao.setValorCapitalAInvestir(scanner.nextDouble());

        System.out.println("Informe a quantidade de meses desejada: ");
        simulacao.setQuantidadeMeses(scanner.nextInt());
    }

}
